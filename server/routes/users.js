var express = require('express');
var router = express.Router();
var user = require('../models/users')

/* GET users listing. */
router.get('/', function (req, res, next) {

  user.find({}, function (err, doc) {
    if (err) return send(err);
    res.send(doc);
  });

});

router.post('/', function ({
  body
}, res, next) {

  user.create(body, function (err) {
    if (err) return console.log(err);

    user.find({}, function (err, users) {
      if (err) return console.log(err);
      res.send(users);
    });

  });



});

router.delete('/', function ({
  body
}, res, next) {

  user.deleteMany({
    _id: {
      $in: body
    }
  }, function (err) {
    if (err) console.log(err)
  })
  user.find({}, function (err, users) {
    if (err) return console.log(err);
    res.send(users);
  });


});

module.exports = router;